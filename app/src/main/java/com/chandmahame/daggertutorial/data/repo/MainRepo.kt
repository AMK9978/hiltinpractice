package com.chandmahame.daggertutorial.data.repo

import com.chandmahame.daggertutorial.data.api.APIHelper
import javax.inject.Inject

class MainRepo @Inject constructor(private val apiHelper: APIHelper) {
    suspend fun getUsers() = apiHelper.getUsers()
}