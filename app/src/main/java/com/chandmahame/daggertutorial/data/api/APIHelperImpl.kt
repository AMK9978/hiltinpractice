package com.chandmahame.daggertutorial.data.api

import com.chandmahame.daggertutorial.data.models.User
import retrofit2.Response
import javax.inject.Inject

class APIHelperImpl @Inject constructor(private val apiService: APIService) : APIHelper {
    override suspend fun getUsers(token:String): Response<List<User>> {
        return apiService.getUsers()
    }

}