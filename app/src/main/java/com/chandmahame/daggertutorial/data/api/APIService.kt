package com.chandmahame.daggertutorial.data.api

import com.chandmahame.daggertutorial.data.models.User
import retrofit2.Response
import retrofit2.http.GET

interface APIService {
    @GET("users")
    suspend fun getUsers(): Response<List<User>>
}