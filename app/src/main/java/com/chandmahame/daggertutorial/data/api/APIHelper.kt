package com.chandmahame.daggertutorial.data.api

import com.chandmahame.daggertutorial.data.models.User
import retrofit2.Response

interface APIHelper {
    suspend fun getUsers(): Response<List<User>>
}