package com.chandmahame.daggertutorial.ui

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.chandmahame.daggertutorial.data.models.User
import com.chandmahame.daggertutorial.data.repo.MainRepo
import com.chandmahame.daggertutorial.utils.NetworkHelper
import com.chandmahame.daggertutorial.utils.Resource
import kotlinx.coroutines.launch
import javax.inject.Singleton

@Singleton
class MainViewModel @ViewModelInject constructor
    (
    private val mainRepository: MainRepo,
    private val networkHelper: NetworkHelper
) : ViewModel() {

    private val _users = MutableLiveData<
            Resource<List<User>>>()
    val users: LiveData<Resource<List<User>>>
        get() = _users

    private fun fetchUsers() {
        viewModelScope.launch {
            _users.postValue(Resource.loading(null))
            if (networkHelper.isNetworkConnected()) {
                mainRepository.getUsers().let {
                    if (it.isSuccessful) {
                        _users.postValue(Resource.success(it.body()))
                    } else {
                        _users.postValue(Resource.error(it.errorBody().toString(), null))
                    }
                }
            } else {
                _users.postValue(Resource.error(("No Internet connection"), null))
            }
        }
    }

    public fun provideToken(token: String) {
        fetchUsers()
    }
}