package com.chandmahame.daggertutorial.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}