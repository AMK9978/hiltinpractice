package com.chandmahame.daggertutorial.utils

import android.content.Context

class Util {
    companion object {
        private const val PREF_NAME = "APP_PREF"
        const val TOKEN = "PREF_TOKEN"

        fun getPrefString(context: Context, key: String): String? {
            return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).getString(key, "")
        }
    }

}