package com.chandmahame.daggertutorial.di

import javax.inject.Qualifier

class Qualifiers {
    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class PersonalName

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class BASE_URL
}