package com.chandmahame.daggertutorial.di

import androidx.viewbinding.BuildConfig
import com.chandmahame.daggertutorial.data.api.APIHelper
import com.chandmahame.daggertutorial.data.api.APIHelperImpl
import com.chandmahame.daggertutorial.data.api.APIService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class AppModule {

    companion object {
        private const val BASE_URL = "https://5f3e2ea413a9640016a68652.mockapi.io/api/"
        private val client: OkHttpClient = OkHttpClient.Builder().addInterceptor {
            val newRequest =
                it.request().newBuilder().addHeader("Authorization", "Bearer " + "token")
                    .build()
            it.proceed(newRequest)
        }.build()
    }

    @Qualifiers.BASE_URL
    @Singleton
    @Provides
    fun provideBaseURL() = BASE_URL

    @Provides
    @Singleton
    fun provideOkHttpClient() = if (BuildConfig.DEBUG) {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level =
            HttpLoggingInterceptor.Level.BODY
        OkHttpClient.Builder().addInterceptor(
            loggingInterceptor
        ).build()
    } else OkHttpClient.Builder().build()

    @Provides
    @Singleton
    fun provideRetrofit(
        okHttpClient: OkHttpClient,
        BASE_URL: String
    ): Retrofit =
        Retrofit.Builder().baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(
                MoshiConverterFactory.create()
            ).build()

    @Provides
    @Singleton
    fun provideAPIService(retrofit: Retrofit) = retrofit.create(APIService::class.java)

    @Provides
    @Singleton
    fun provideAPIHelper(apiHelperImpl: APIHelperImpl)
            : APIHelper = apiHelperImpl

    @Provides
    @Singleton
    fun provideToken(token: String): String = token

    @Qualifiers.PersonalName
    @Provides
    @Singleton
    fun providePersonalName(name: String): String = name

}